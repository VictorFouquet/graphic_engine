#include "editorCamera.h"
#include "keyCode.h"
#include "input.h"

#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/quaternion.hpp"


namespace GraphicEngine
{
    EditorCamera::EditorCamera(float fov, float aspectRatio, float nearClip, float farClip)
        : _FOV(fov), _aspectRatio(aspectRatio), _nearClip(nearClip), _farClip(farClip), Camera(glm::perspective(glm::radians(fov), aspectRatio, nearClip, farClip))
    {
        
    }
    
    void EditorCamera::onUpdate(Timestep ts) 
    {
        if (Input::isKeyPressed(Key::ALT_LEFT))
        {
            const glm::vec2& mouse{ Input::getMouseX(), Input::getMouseY() };
            glm::vec2 delta = (mouse - _initialMousePosition) * 0.0003f;
            _initialMousePosition = mouse;

            if (Input::isMouseButtonPressed(2))
                mousePan(delta);
            else if (Input::isMouseButtonPressed(0))
                mouseRotate(delta);
            else if (Input::isMouseButtonPressed(1))
                mouseZoom(delta.y);
        }

        updateView();
    }
    
    void EditorCamera::onEvent(Event& e) 
    {
        EventDispatcher dispatcher(e);
        dispatcher.dispatch<MouseScrolledEvent>(BIND_EVENT_FN(EditorCamera::onMouseScroll));
    }
    
    glm::vec3 EditorCamera::getUpDirection() const
    {
        return glm::rotate(getOrientation(), glm::vec3(0.0f, 1.0f, 0.0f));
    }
    
    glm::vec3 EditorCamera::getRightDirection() const
    {
        return glm::rotate(getOrientation(), glm::vec3(1.0f, 0.0f, 0.0f));
    }
    
    glm::vec3 EditorCamera::getForwardDirection() const
    {
        return glm::rotate(getOrientation(), glm::vec3(0.0f, 0.0f, -1.0f));
    }
    
    glm::quat EditorCamera::getOrientation() const
    {
        return glm::quat(glm::vec3(-_pitch, -_yaw, 0.0f));
    }

    glm::vec3 EditorCamera::calculatePosition() const
    {
        return _focalPoint - getForwardDirection() * _distance;
    }

    void EditorCamera::updateProjection()
    {
        _aspectRatio = _viewportWidth / _viewportHeight;
        _projection = glm::perspective(glm::radians(_FOV), _aspectRatio, _nearClip, _farClip);
    }

    void EditorCamera::updateView()
    {
        // _yaw = _pitch = 0.0f;// Uncomment to lock camera's rotation
        _position = calculatePosition();

        glm::quat orientation = getOrientation();
        _viewMatrix = glm::translate(glm::mat4(1.0f), _position) * glm::toMat4(orientation);
        _viewMatrix = glm::inverse(_viewMatrix);
    }

    bool EditorCamera::onMouseScroll(MouseScrolledEvent& e)
    {
        float delta = e.getYOffset() * 0.1f;
        mouseZoom(delta);
        updateView();

        return false;
    }

    void EditorCamera::mousePan(const glm::vec2& delta)
    {
        auto [xSpeed, ySpeed] = panSpeed();
        _focalPoint += - getRightDirection() * delta.x * xSpeed * _distance;
        _focalPoint += getUpDirection() * delta.y * ySpeed * _distance;
    }

    void EditorCamera::mouseRotate(const glm::vec2& delta)
    {
        float yawSign = getUpDirection().y < 0 ? -1.0f : 1.0f;
        _yaw += yawSign * delta.x * rotationSpeed();
        _pitch += delta.y * rotationSpeed();
    }
    
    void EditorCamera::mouseZoom(float delta) 
    {
        _distance -= delta * zoomSpeed();
        
        if (_distance < 1.0f)
        {
            _focalPoint += getForwardDirection();
            _distance = 1.0f;
        }
    }
    
    std::pair<float, float> EditorCamera::panSpeed() const
    {
        // Quadratic function are used to compute pan speed

        float x = std::min(_viewportWidth / 1000.0f, 4.8f);
        float xFactor = 0.366f * (x * x) - 0.1778f * x + 0.3021f;

        float y = std::min(_viewportHeight / 1000.0f, 4.8f);
        float yFactor = 0.366f * (x * y) - 0.1778f * y + 0.3021f;

        return { xFactor, yFactor };
    }
    
    float EditorCamera::rotationSpeed() const
    {
        return 2.4f;
    }
    
    float EditorCamera::zoomSpeed() const
    {
        float distance = _distance * 0.8f;
        distance = std::max(distance, 0.0f);
        float speed = distance * distance;
        speed = std::min(speed, 50.0f);
        
        return speed;
    }
}